package Controlador;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;
import Vistas.dlgRegistros;
import Modelos.Registros;
import Modelos.dbRegistros;
import java.util.Date;
public class Controlador implements ActionListener{
    private dlgRegistros vista;
    private Registros R;
    private dbRegistros DR;
    private boolean option = false;

    public Controlador(dlgRegistros vista, Registros R, dbRegistros DR) {
        this.vista = vista;
        this.R = R;
        this.DR = DR;
        
        vista.btnBuscar.addActionListener(this);
        vista.btnCancelar.addActionListener(this);
        vista.btnCerrar.addActionListener(this);
        vista.btnDeshabilitar.addActionListener(this);
        vista.btnGuardar.addActionListener(this);
        vista.btnLimpiar.addActionListener(this);
        vista.btnNuevo.addActionListener(this);
    }
    
    private void iniciarVentana(){
        limpiar();
        vista.setTitle(":: VENTAS ::");
        vista.setSize(730,650);
        vista.setLocationRelativeTo(null);
        vista.setVisible(true);
    }
    
    private void limpiar() {
        vista.txtEmail.setText("");
        vista.txtNombre.setText("");
        vista.txtNum.setText("");
        vista.cmbEventos.setSelectedIndex(0);
    }
    
   
    public static void main(String[] args) {
        Registros R = new Registros();
        dbRegistros DR = new dbRegistros();
        dlgRegistros vista = new dlgRegistros(new JFrame(), true);
        Controlador contra = new Controlador(vista, R, DR);
        contra.iniciarVentana();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource() == vista.btnBuscar){
            try {
                Registros R = new Registros();
                if (DR.isExiste(vista.txtNum.getText())) {
                    R = (Registros) DR.buscar(vista.txtNum.getText());
                    if(R.getStatus() == 1){
                        JOptionPane.showMessageDialog(vista, "ESTE REGISTRO ESTÁ DESHABILITADO");
                        return;
                    }
                    System.out.println(R.getStatus());
                    vista.txtNum.setEnabled(false);
                    vista.txtNombre.setText(R.getNombre());
                    vista.txtNum.setText(R.getNumAf());
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                    Date fecha = sdf.parse(R.getFecha());
                    vista.jdFecha.setDate(fecha);
                    vista.txtEmail.setText(R.getEmail());
                    if (R.getEvento()== 0) {
                        vista.cmbEventos.setSelectedIndex(0);
                    }
                    if (R.getEvento()== 1) {
                        vista.cmbEventos.setSelectedIndex(1);
                    }
                    if (R.getEvento()== 2) {
                        vista.cmbEventos.setSelectedIndex(2);
                    }
                    if (R.getEvento()== 3) {
                        vista.cmbEventos.setSelectedIndex(3);
                    }
                    if (R.getEvento()== 4) {
                        vista.cmbEventos.setSelectedIndex(4);
                    }
                    JOptionPane.showMessageDialog(vista, "Se localizo");
                    option = true;
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(vista, "NO EXISTE EL REGISTRO");
                System.out.println("Surgió un error" + ex.getMessage());
            }
        }
        
         if (e.getSource() == vista.btnGuardar) {
            try{
                if(option == true){
                   int choice = JOptionPane.showConfirmDialog(vista, "¿ACTUALIZAR?");
                   if(choice == 0){
                       R.setNombre(vista.txtNombre.getText());
                       R.setNumAf(vista.txtNum.getText());
                       R.setEmail(vista.txtEmail.getText());
                       R.setEvento(vista.cmbEventos.getSelectedIndex());
                       SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                       String date = sdf.format(vista.jdFecha.getDate());
                       R.setFecha(date);
                            if(R.getEvento()== 0){
                                R.setEvento(0);
                            }
                            if(R.getEvento()== 1){
                                R.setEvento(1);
                            }
                            if(R.getEvento()== 2){
                                R.setEvento(2);
                            }
                            if(R.getEvento()== 3){
                                R.setEvento(3);
                            }
                            if(R.getEvento()== 4){
                                R.setEvento(4);
                            }
                       DR.actualizar(R);
                       limpiar();
                   }
                   option = false;
                   limpiar();
                   return;
            }
                if(!vista.txtNum.getText().equals("") && DR.isExiste(vista.txtNum.getText()) == false){
                    R.setNombre(vista.txtNombre.getText());
                    R.setNumAf(vista.txtNum.getText());
                    R.setEmail(vista.txtEmail.getText());
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                    String date = sdf.format(vista.jdFecha.getDate());
                    R.setFecha(date);
                    R.setEvento(vista.cmbEventos.getSelectedIndex());
                        if(R.getEvento()== 0){
                                R.setEvento(0);
                            }
                            if(R.getEvento()== 1){
                                R.setEvento(1);
                            }
                            if(R.getEvento()== 2){
                                R.setEvento(2);
                            }
                            if(R.getEvento()== 3){
                                R.setEvento(3);
                            }
                            if(R.getEvento()== 4){
                                R.setEvento(4);
                            }
                    JOptionPane.showMessageDialog(vista, "AGREGADO");
                    DR.insertar(R);
                    limpiar();       
                }
                else{
                    JOptionPane.showMessageDialog(vista, "VALOR INVÁLIDO");
                }
            }
            catch(Exception ex){
                System.out.println("Surgió un error" + ex.getMessage());
            }
        }
        
        if(e.getSource() == vista.btnCerrar){
            int out = JOptionPane.showConfirmDialog(vista, "¿Desea salir del programa?", "CERRAR PROGRAMA",JOptionPane.OK_CANCEL_OPTION);
            if(out == JOptionPane.OK_OPTION){
                System.exit(0); 
            }
        }
        
        if(e.getSource() == vista.btnDeshabilitar){
            try{
                DR.deshablitar(R, vista.txtNum.getText());
                JOptionPane.showMessageDialog(vista, "REGISTRO DESHABILITADO");
            }
            catch(Exception ex){
                System.out.println("Surgió un error: " + ex.getMessage());
            }
        }
        
        if(e.getSource() == vista.btnLimpiar){
            limpiar();
        }
        
        if(e.getSource() == vista.btnNuevo){
            limpiar();
            vista.txtNum.setEnabled(true);
            vista.cmbEventos.setEnabled(true);
            vista.btnGuardar.setEnabled(true);
            vista.btnCancelar.setEnabled(true);
            vista.btnNuevo.setEnabled(false);
        }
        
        if(e.getSource() == vista.btnCancelar){
            vista.btnLimpiar.doClick();
            vista.btnGuardar.setEnabled(false);
            vista.cmbEventos.setEnabled(false);
            vista.btnNuevo.setEnabled(true);
        }
        
        
    }
    
}
