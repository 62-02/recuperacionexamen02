/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelos;

/**
 *
 * @author felip
 */
public class Registros {
    private double id;
    private String numAf;
    private int evento;
    private String fecha;
    private String email;
    private int status;
    private String nombre;

    public Registros(double id, String numAf, int evento, String fecha, String email, int status, String nombre) {
        this.id = id;
        this.numAf = numAf;
        this.evento = evento;
        this.fecha = fecha;
        this.email = email;
        this.status = status;
        this.nombre = nombre;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Registros() {
    }

    public double getId() {
        return id;
    }

    public void setId(double id) {
        this.id = id;
    }

    public String getNumAf() {
        return numAf;
    }

    public void setNumAf(String numAf) {
        this.numAf = numAf;
    }

    public int getEvento() {
        return evento;
    }

    public void setEvento(int evento) {
        this.evento = evento;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
