/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelos;

import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.JOptionPane;
/**
 *
 * @author felip
 */
public class dbRegistros extends dbManejador implements dbPersistencia{
    @Override
    public void insertar(Object objeto) throws Exception {
        Registros reg = new Registros();
        reg = (Registros) objeto;
        String consulta = "insert into " + "anfrition02(numAf, nombre, evento, fecha, email, status) values (?, ?, ?, ?, ?, ?)";
        if(this.conectar()){
            try{
                System.err.println("Se conecto");
                this.sqlConsulta = conexion.prepareStatement(consulta);
                
                this.sqlConsulta.setString(1, reg.getNumAf());
                this.sqlConsulta.setString(2, reg.getNombre());
                this.sqlConsulta.setInt(3, reg.getEvento());
                this.sqlConsulta.setString(4, reg.getFecha());
                this.sqlConsulta.setString(5,reg.getEmail());
                this.sqlConsulta.setInt(6, 0);
                this.sqlConsulta.executeUpdate();
                this.desconectar();
            }
            catch (SQLException e){
                System.err.println("Surgió un error al insertar" + e.getMessage());
            }
        }
        else{
            System.out.println("No fue posible conectarse ");
        }
    }

    @Override
    public void actualizar(Object objeto) throws Exception {
        Registros reg = new Registros();
        reg = (Registros) objeto;
        String consulta = "update anfrition02 set nombre = ?, evento = ?, fecha = ?, email = ? where numAf = ?";
        if(this.conectar()){
            try{
                System.err.println("Se conecto");
                this.sqlConsulta = conexion.prepareStatement(consulta);
                
                this.sqlConsulta.setString(1, reg.getNombre());
                this.sqlConsulta.setInt(2, reg.getEvento());
                this.sqlConsulta.setString(3, reg.getFecha());
                this.sqlConsulta.setString(4, reg.getEmail());
                this.sqlConsulta.setString(5, reg.getNumAf());
                
                this.sqlConsulta.executeUpdate();
                this.desconectar();
            }
            catch (SQLException e){
                System.err.println("Surgió un error al actualizar" + e.getMessage());
            }
        }
        else{
            System.out.println("No fue posible conectarse ");
        }
    }

    @Override
    public boolean isExiste(String codigo) throws Exception {
        boolean confirm = false;
        if(this.conectar()){
            String consulta = "Select * from anfrition02 where numAf = ?";
            this.sqlConsulta = conexion.prepareStatement(consulta);
            this.sqlConsulta.setString(1, codigo);
            this.registros = this.sqlConsulta.executeQuery();
            if(this.registros.next()){
                confirm = true;
            }
        }
        this.desconectar();
        return confirm;
    }

    @Override
    public Object buscar(String codigo) throws Exception {
        Registros reg = new Registros();
        if(conectar()){
            String consulta = "SELECT * from anfrition02 WHERE numAf = ?";
            this.sqlConsulta = this.conexion.prepareStatement(consulta);
            this.sqlConsulta.setString(1, codigo);

            this.registros = this.sqlConsulta.executeQuery();
            if(this.registros.next()){
                reg.setNumAf(this.registros.getString("numAf"));
                reg.setNombre(this.registros.getString("nombre"));
                reg.setEvento(this.registros.getInt("evento"));
                reg.setFecha(this.registros.getString("fecha"));
                reg.setEmail(this.registros.getString("email"));
                reg.setStatus(this.registros.getInt("status"));
            }
        }
        this.desconectar();
        return reg;
    }
    
    @Override
    public void deshablitar(Object object,String codigo) throws Exception {
        Registros reg = new Registros();
        reg = (Registros) object;
            String consulta ="update anfrition02 set status = 1 where numAf = ?";
            if(this.conectar()){
                try{
                System.err.println("Se conecto");
                this.sqlConsulta = conexion.prepareStatement(consulta);

                //asignar los valores ala consulta

                this.sqlConsulta.setString(1, codigo);
                this.sqlConsulta.executeUpdate();
                this.desconectar();
            } catch(SQLException e){
               System.err.println("Surgio un error al insertar"+ e.getMessage());
            }
        }
    }
}